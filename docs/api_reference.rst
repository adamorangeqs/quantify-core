.. jupyter-kernel:: python3
    :id: quantify_core_all_docs

.. jupyter-execute::
    :hide-code:

    # Prettify the outputs in the entire API reference
    from rich import pretty
    pretty.install()

=============
quantify_core
=============

.. automodule:: quantify_core
   :members:

.. _analysis_api:

analysis
========

.. automodule:: quantify_core.analysis
    :members:


base_analysis
-------------

.. automodule:: quantify_core.analysis.base_analysis
    :members:
    :show-inheritance:

cosine_analysis
---------------

.. automodule:: quantify_core.analysis.cosine_analysis
    :members:
    :show-inheritance:

spectroscopy_analysis
---------------------

.. automodule:: quantify_core.analysis.spectroscopy_analysis
    :members:
    :show-inheritance:

single_qubit_timedomain
-----------------------

.. automodule:: quantify_core.analysis.single_qubit_timedomain
    :members:
    :show-inheritance:


interpolation_analysis
----------------------

.. automodule:: quantify_core.analysis.interpolation_analysis
    :members:
    :show-inheritance:

optimization_analysis
---------------------

.. automodule:: quantify_core.analysis.optimization_analysis
    :members:
    :show-inheritance:


fitting_models
--------------

.. automodule:: quantify_core.analysis.fitting_models
    :members:
    :show-inheritance:


calibration
-----------

.. automodule:: quantify_core.analysis.calibration
    :members:
    :show-inheritance:


data
====

types
-----

.. automodule:: quantify_core.data.types
    :members:

handling
--------

.. automodule:: quantify_core.data.handling
    :members:

dataset_adapters
----------------

.. automodule:: quantify_core.data.dataset_adapters
    :members:

dataset_attrs
-------------

.. automodule:: quantify_core.data.dataset_attrs
    :members:


measurement
===========

.. automodule:: quantify_core.measurement
    :members:

types
-----

.. automodule:: quantify_core.measurement.types
    :members:

control
-------

.. automodule:: quantify_core.measurement.control
    :members:


utilities
=========

experiment_helpers
------------------

.. automodule:: quantify_core.utilities.experiment_helpers
    :members:

dataset_examples
----------------

.. automodule:: quantify_core.utilities.dataset_examples
    :members:

examples_support
----------------

.. automodule:: quantify_core.utilities.examples_support
    :members:

visualization
=============

.. automodule:: quantify_core.visualization
    :members:

instrument_monitor
------------------

.. automodule:: quantify_core.visualization.instrument_monitor
    :members:

pyqt_plotmon
------------

.. automodule:: quantify_core.visualization.pyqt_plotmon
    :members:

color_utilities
---------------

.. automodule:: quantify_core.visualization.color_utilities
    :members:

mpl_plotting
------------

.. automodule:: quantify_core.visualization.mpl_plotting
    :members:

plot_interpolation
------------------

.. automodule:: quantify_core.visualization.plot_interpolation
    :members:

SI Utilities
------------

.. automodule:: quantify_core.visualization.SI_utilities
    :members:

============
bibliography
============

.. bibliography::
