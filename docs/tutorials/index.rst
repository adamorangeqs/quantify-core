=========
Tutorials
=========


.. toctree::
   :maxdepth: 1

   Tutorial 1. Controlling a basic experiment using MeasurementControl.py.rst
   Tutorial 2. Advanced capabilities of the MeasurementControl.py.rst
   Tutorial 3. Building custom analyses - the data analysis framework.py.rst
   Tutorial 4. Adaptive Measurements.py.rst
   Tutorial 5. Plot monitor.py.rst
