Dataset design
==============

In this part of the documentation, you will find the dataset specification, starting with a brief overview of :doc:`Xarray <xarray:index>` on which the Quantify dataset is based, and examples of datasets that comply with that specification. Along with that, you will also find some information on the design choices and known limitations.

.. toctree::
   :maxdepth: 1
   :caption: Dataset design

   Xarray introduction.py.rst
   Quantify dataset - specification.py.rst
   Quantify dataset - examples.py.rst
   Quantify dataset - advanced examples.py.rst
