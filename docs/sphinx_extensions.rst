sphinx_extensions
=================

Here we document the parts of the code that can be used as sphinx extensions. These sphinx extensions have been created to support the documentation build workflow for this project and related repositories.

.. warning::

    In the future, this module is subject to being factored out from this project since it could benefit the `sphinx` and `jupyter_sphinx` community.

notebook_to_jupyter_sphinx
--------------------------

.. automodule:: quantify_core.sphinx_extensions.notebook_to_jupyter_sphinx
    :members:
