# sphinx warnings to be ignored, see `nitpick_ignore` in conf.py for details
# https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-nitpicky

# Please always add comments justifying the warning ignore

# A type hint from typing_extensions that sphinx and its extensions do not handle well
py:class Literal[None, running, interrupted (safety), interrupted (forced), done]
py:class typing_extensions.Literal[None, running, interrupted (safety), interrupted (forced), done]
py:class dataclasses_json.api.DataClassJsonMixin
